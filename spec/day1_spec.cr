require "./spec_helper"

describe Day1 do
  it "part1" do
    Day1.test1("12").should eq 2
    Day1.test1("14").should eq 2
    Day1.test1("1969").should eq 654
    Day1.test1("100756").should eq 33583
  end

  it "part2" do
    Day1.test2("14").should eq 2
    Day1.test2("1969").should eq 966
    Day1.test2("100756").should eq 50346
  end
end
