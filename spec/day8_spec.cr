require "./spec_helper"

describe Day8 do
  it "part1" do
    Day8.checksum(3, 2, "123456789012").should eq 1
  end

  it "part2" do
    Day8.decode(2, 2, "0222112222120000").should eq "
 █
█ "
  end
end
