require "./spec_helper"

describe Day5 do
  it "part1" do
    [1002, 4, 3, 4, 33].tap { |x| IntCode.async(x, nil, nil) }.should eq [1002, 4, 3, 4, 99]
    Day5.test1("3,0,4,0,99").should eq 1
  end

  it "part2" do
    IntCode.execute([3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], [1]).should eq 0
    IntCode.execute([3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], [8]).should eq 1

    IntCode.execute([3, 3, 1108, -1, 8, 3, 4, 3, 99], [7]).should eq 0
    IntCode.execute([3, 3, 1108, -1, 8, 3, 4, 3, 99], [8]).should eq 1

    IntCode.execute([3, 3, 1107, -1, 8, 3, 4, 3, 99], [7]).should eq 1
    IntCode.execute([3, 3, 1107, -1, 8, 3, 4, 3, 99], [8]).should eq 0

    IntCode.execute([3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9], [0]).should eq 0
    IntCode.execute([3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9], [2]).should eq 1
    IntCode.execute([3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1], [10]).should eq 1
    IntCode.execute([3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1], [0]).should eq 0

    prg = [3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31,
           1106, 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104,
           999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99]
    IntCode.execute(prg, [0]).should eq 999
    IntCode.execute(prg, [8]).should eq 1000
    IntCode.execute(prg, [9]).should eq 1001
  end
end
