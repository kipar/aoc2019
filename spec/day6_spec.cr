require "./spec_helper"

describe Day6 do
  it "part1" do
    Day6.test1("COM)B").should eq 1
    Day6.test1("COM)A
A)B").should eq 3

    Day6.test1("COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L").should eq 42

    Day6.test1("D)E
E)F
B)G
J)K
B)C
C)D
K)L
G)H
D)I
COM)B
E)J").should eq 42
  end

  it "part2" do
    Day6.test2("COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN").should eq 4
  end
end
