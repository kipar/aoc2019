require "./spec_helper"

describe Day4 do
  it "part1" do
    Day4.check(111111).should be_true
    Day4.check(223450).should be_false
    Day4.check(123789).should be_false
    Day4.check(123789).should be_false
    Day4.test1("111111-111112").should eq 2
    Day4.test1("123787-123789").should eq 1
  end

  it "part2" do
    Day4.check2(112233).should be_true
    Day4.check2(123444).should be_false
    Day4.check2(111122).should be_true
  end
end
