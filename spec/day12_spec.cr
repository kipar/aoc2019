require "./spec_helper"

describe Day12 do
  it "part1" do
    Day12.calc_energy((<<-STRING
    <x=-1, y=0, z=2>
    <x=2, y=-10, z=-7>
    <x=4, y=-8, z=8>
    <x=3, y=5, z=-1>
    STRING

      ), 10).should eq 179

    Day12.calc_energy((<<-STRING
    <x=-8, y=-10, z=0>
    <x=5, y=5, z=10>
    <x=2, y=-7, z=3>
    <x=9, y=-8, z=-3>
    STRING

      ), 100).should eq 1940
  end

  it "part2" do
    Day12.test2(<<-STRING
    <x=-1, y=0, z=2>
    <x=2, y=-10, z=-7>
    <x=4, y=-8, z=8>
    <x=3, y=5, z=-1>
    STRING
    ).should eq 2772
    Day12.test2(<<-STRING
    <x=-8, y=-10, z=0>
    <x=5, y=5, z=10>
    <x=2, y=-7, z=3>
    <x=9, y=-8, z=-3>
    STRING
    ).should eq 4686774924
  end
end
