require "./spec_helper"

describe Day2 do
  it "part1" do
    Day2.execute([1, 0, 0, 0, 99]).should eq [2, 0, 0, 0, 99]
    Day2.execute([2, 3, 0, 3, 99]).should eq [2, 3, 0, 6, 99]
    Day2.execute([2, 4, 4, 5, 99, 0]).should eq [2, 4, 4, 5, 99, 9801]
    Day2.execute([1, 1, 1, 4, 99, 5, 6, 0, 99]).should eq [30, 1, 1, 4, 2, 5, 6, 0, 99]
  end

  it "part2" do
    Day2.part2("6568671").should eq 1202
  end
end
