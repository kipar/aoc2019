require "./spec_helper"

describe Day3 do
  it "part1" do
    Day3.test1("R8,U5,L5,D3
U7,R6,D4,L4").should eq 6
    Day3.test1("R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83").should eq 159
    Day3.test1("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7").should eq 135
  end

  it "part2" do
    Day3.test2("R8,U5,L5,D3
U7,R6,D4,L4").should eq 30
    Day3.test2("R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83").should eq 610
    Day3.test2("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7").should eq 410
  end
end
