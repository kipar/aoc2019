require "./spec_helper"

describe Day16 do
  it "part1" do
    Day16.fft("12345678", 1).should eq "48226158"
    Day16.fft("12345678", 4).should eq "01029498"

    Day16.test1("80871224585914546619083218645595").should eq "24176176"
    Day16.test1("19617804207202209144916044189917").should eq "73745418"
    Day16.test1("69317163492948606335995924319873").should eq "52432133"
  end

  it "part2" do
    DayN.test2("").should eq "part2"
  end
end
