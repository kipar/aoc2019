require "./spec_helper"

describe Day9 do
  it "part1" do
    IntCode.execute_arr([109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99]).should eq [109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99]
    IntCode.execute([1102, 34915192, 34915192, 7, 4, 7, 99, 0].map(&.to_i64)).should eq 1219070632396864
    IntCode.execute([104, 1125899906842624, 99].map(&.to_i64)).should eq 1125899906842624
  end
end
