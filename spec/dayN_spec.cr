require "./spec_helper"

describe DayN do
  it "part1" do
    DayN.test1("").should eq "part1"
  end

  it "part2" do
    DayN.test2("").should eq "part2"
  end
end
