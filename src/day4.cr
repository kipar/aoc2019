require "./aoc2019"

module Day4
  include Day
  extend self

  def check(number)
    digits = number.to_s.chars.map(&.ord)
    double = false
    digits.each_cons(2, reuse: true) do |(first, second)|
      return false if second < first
      double = true if first == second
    end
    double
  end

  def part1(input)
    v1, v2 = input.split('-').map(&.to_i)
    (v1..v2).count { |x| check(x) }
  end

  def check2(number)
    digits = number.to_s.chars.map(&.ord)
    match = 0
    was_double = false
    digits.each_cons(2, reuse: true) do |(first, second)|
      return false if second < first
      if first == second
        match += 1
      else
        was_double = true if match == 1
        match = 0
      end
    end
    was_double || match == 1
  end

  def part2(input)
    v1, v2 = input.split('-').map(&.to_i)
    (v1..v2).count { |x| check2(x) }
  end
end
