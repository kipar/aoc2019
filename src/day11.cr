require "./aoc2019"
require "./intcode"

module Day11
  include Day
  extend self

  DX = {0, 1, 0, -1}
  DY = {-1, 0, 1, 0}

  def call(input, initial)
    prg = input.split(',').map(&.to_i64)
    chcolor = Channel(Int64).new(1)
    chcommands = Channel(Int64?).new(2)
    spawn do
      IntCode.async(prg, chcolor, chcommands)
      chcommands.send(nil)
    end
    map = Hash(Tuple(Int32, Int32), Bool).new(false)
    pos = {0, 0}
    map[pos] = initial
    dir = 0
    loop do
      # brain
      chcolor.send(map[pos] ? 1i64 : 0i64)
      color = chcommands.receive
      break unless color
      # paint
      map[pos] = color == 1
      # puts map
      # turn
      turn = chcommands.receive.not_nil!
      if turn == 0
        dir = (dir + 3) % 4
      else
        dir = (dir + 1) % 4
      end
      # move
      pos = {pos[0] + DX[dir], pos[1] + DY[dir]}
    end
    map
  end

  def part1(input)
    map = call(input, false)
    map.keys.size
  end

  def part2(input)
    map = call(input, true)
    positions = map.keys
    minx, maxx = positions.minmax_of { |(x, y)| x }
    miny, maxy = positions.minmax_of { |(x, y)| y }

    String.build do |str|
      (miny..maxy).each do |y|
        str << "\n"
        (minx..maxx).each do |x|
          str << (map[{x, y}] ? '█' : ' ')
        end
      end
    end
  end
end
