require "./aoc2019"

module Day12
  include Day
  extend self

  class Moon
    getter pos
    getter vel

    def initialize(string)
      _, x, y, z = /<x=(.*), y=(.*), z=(.*)>/.match(string).not_nil!
      @pos = StaticArray(Int32, 3).new(0)
      @pos[0], @pos[1], @pos[2] = x.to_i, y.to_i, z.to_i
      @vel = StaticArray(Int32, 3).new(0)
    end

    def pos
      pointerof(@pos).unsafe_as(Pointer(Int32))
    end

    def vel
      pointerof(@vel).unsafe_as(Pointer(Int32))
    end

    def energy
      @pos.sum(&.abs) * @vel.sum(&.abs)
    end

    def dump
      puts "[pos=#{@pos}, vel=#{@vel}, kin=#{@vel.sum(&.abs)}, pot=#{@pos.sum(&.abs)}]"
    end
  end

  def calc_energy(input, steps)
    moons = input.split('\n').map { |s| Moon.new s }
    steps.times do |step|
      # update velocity
      moons.each_with_index do |m1, i|
        (i + 1...moons.size).each do |j|
          m2 = moons[j]
          3.times do |c|
            grav = (m1.pos[c] <=> m2.pos[c])
            m1.vel[c] -= grav
            m2.vel[c] += grav
          end
        end
      end
      # update pos
      moons.each do |m|
        3.times { |c| m.pos[c] += m.vel[c] }
      end
      # puts "step #{step} sum=#{moons.sum(&.energy)}"
      # moons.each &.dump
    end
    moons.sum(&.energy)
  end

  def find_period(input)
    moons = input.split('\n').map { |s| Moon.new s }

    osc = [] of Int64
    3.times do |c|
      initialpos = moons.map &.pos[c]
      pos = initialpos.dup
      vel = Array(Int32).new(4, 0)
      step = 0i64
      loop do
        step += 1

        4.times do |i|
          (i + 1...4).each do |j|
            g = pos[i] <=> pos[j]
            vel[i] -= g
            vel[j] += g
          end
        end
        4.times { |i| pos[i] += vel[i] }
        break if vel.all?(&.==(0)) && pos == initialpos
      end
      osc << step
    end
    # puts osc
    osc[0].lcm(osc[1]).lcm(osc[2])
  end

  def part1(input)
    calc_energy(input, 1000)
  end

  def part2(input)
    find_period input
  end
end
