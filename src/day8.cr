require "./aoc2019"

module Day8
  include Day
  extend self

  def checksum(w, h, data)
    best0 = w*h
    best12 = -1
    data.chars.each_slice(w*h, true) do |arr|
      v = arr.tally
      if (v['0']? || 0) < best0
        best12 = v['1']*v['2']
        if v['0']?
          best0 = v['0']
        else
          # best0 = 0
          break
        end
      end
    end
    best12
  end

  VISUALS = {'0' => ' ', '1' => '█', '2' => '2'}

  def decode(w, h, data)
    image = Array(Char).new(w*h, '2')
    data.chars.each_slice(w*h, true) do |arr|
      arr.each_with_index do |c, i|
        image[i] = VISUALS[c] if image[i] == '2'
      end
    end
    String.build do |str|
      image.each_slice(w) { |row| str << '\n' << row.join }
    end
  end

  def part1(input)
    checksum(25, 6, input)
  end

  def part2(input)
    decode(25, 6, input)
  end
end
