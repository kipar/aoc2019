require "./aoc2019"

module Day3
  include Day
  extend self

  alias Point = {Int32, Int32}
  enum Dir
    Up
    Down
    Left
    Right
  end

  class Segment
    getter start : Point
    getter dir : Dir
    getter rate : Int32
    getter size : Int32

    def initialize(@start, @rate, s)
      _, dir, num = s.match(/(.)(.*)/).not_nil!
      @dir = case dir
             when "U"; Dir::Up
             when "D"; Dir::Down
             when "L"; Dir::Left
             when "R"; Dir::Right
             else      raise "incorrect input: #{s}, #{dir}"
             end
      @size = num.to_i
    end

    def vertical?
      @dir.up? || @dir.down?
    end

    def inspect(io)
      io << "UDLR".chars[@dir.to_i] << @size.to_s << "( " << @start << " - " << finish << ")"
    end

    def intersects(other)
      return nil if vertical? == other.vertical?
      return other.intersects self unless vertical?
      mx, my1, my2 = start[0], start[1], finish[1]
      ox1, ox2, oy = other.start[0], other.finish[0], other.start[1]
      return nil if mx == 0 && oy == 0
      my1, my2 = my2, my1 if my2 < my1
      ox1, ox2 = ox2, ox1 if ox2 < ox1
      return nil unless my1 <= oy <= my2
      return nil unless ox1 <= mx <= ox2
      {mx, oy, (oy - start[1]).abs + @rate + (mx - other.start[0]).abs + other.rate}
    end

    def finish
      x, y = @start
      case @dir
      when .up?
        {x, y - @size}
      when .down?
        {x, y + @size}
      when .left?
        {x - @size, y}
      when .right?
        {x + @size, y}
      else
        raise ""
      end
    end
  end

  def to_wires(input)
    input.split('\n').map do |w|
      start = {0, 0}
      rate = 0
      w.split(',').map { |s| Segment.new(start, rate, s).tap { |seg| start = seg.finish; rate += seg.size } }
    end
  end

  def part1(input)
    wire1, wire2 = to_wires(input)
    intersections = [] of Int32
    wire1.each do |seg1|
      wire2.each do |seg2|
        if x = seg1.intersects seg2
          intersections << x[0].abs + x[1].abs
        end
      end
    end
    intersections.min
  end

  def part2(input)
    wire1, wire2 = to_wires(input)
    intersections = [] of Int32
    wire1.each do |seg1|
      wire2.each do |seg2|
        if x = seg1.intersects seg2
          intersections << x[2]
        end
      end
    end
    intersections.min
  end
end
