require "./aoc2019"
require "./intcode"

module Day15
  include Day
  extend self

  alias Point = Tuple(Int32, Int32)
  DX = {0, 0, -1, 1}
  DY = {-1, 1, 0, 0}

  class Map
    getter data = Hash(Point, Bool).new
    property start = {0, 0}
    property pos = {0, 0}
    getter final : Point?
    getter maxdist = 0

    def one_step(dir, status)
      target = {pos[0] + DX[dir], pos[1] + DY[dir]}
      puts "move #{dir} (#{pos} -> #{target}): #{status}" if @log
      case status
      when 0
        @data[target] = true
      when 1
        @data[target] = false
        @pos = target
      when 2
        @pos = target
        @final = target
        @data[target] = false
      end
    end

    property log = false

    def find_way(initial : Point, &block)
      list = Hash(Point, NamedTuple(dist: Int32, from: Point, dir: Int32)).new
      list[initial] = {dist: 0, from: pos, dir: 0}
      open = [initial]
      loop do
        if open.size == 0
          @maxdist = list.values.max_of &.[:dist]
          return [] of Int32
        end
        pos = open.min_by { |it| list[it][:dist] }
        raise "whywhy" if yield(pos)
        open.delete pos
        4.times do |dir|
          step = {pos[0] + DX[dir], pos[1] + DY[dir]}
          p "checking #{step}" if @log
          p "wall" if @data[step]? && @log
          next if @data[step]?
          p "already" if list[step]? && @log
          next if list[step]?
          if yield(step)
            # backtrack
            way = [dir]
            loop do
              way.unshift list[pos][:dir] unless pos == initial
              pos = list[pos][:from]
              puts "found #{way}" if @log && pos == initial
              return way if pos == initial
            end
          else
            open << step
            puts "added #{step}, so #{open}" if @log
            list[step] = {dist: list[pos][:dist] + 1, from: pos, dir: dir}
          end
        end
      end
    end

    def explore(&block)
      @data[@start] = false
      @pos = @start
      loop do
        # puts draw
        way = find_way(@pos) { |(x, y)| @data[{x, y}]?.nil? }
        return if way.size == 0
        way.each do |dir|
          st = yield(dir)
          one_step(dir, st)
        end
      end
    end

    def draw(optional = nil)
      positions = @data.keys
      minx, maxx = positions.minmax_of { |(x, y)| x }
      miny, maxy = positions.minmax_of { |(x, y)| y }
      String.build do |str|
        (miny..maxy).each do |y|
          str << "\n"
          (minx..maxx).each do |x|
            if optional && optional[{x, y}]?
              str << 'X'
            elsif {x, y} == @final
              str << '$'
            elsif {x, y} == @start
              str << '@'
            elsif @data[{x, y}]?.nil?
              str << '?'
            else
              str << (@data[{x, y}] ? '█' : '.')
            end
          end
        end
      end
    end
  end

  def part1(input)
    prg = input.split(',').map(&.to_i)
    chstatus = Channel(Int32).new
    chmove = Channel(Int32).new

    spawn { IntCode.async(prg, chmove, chstatus) }

    map = Map.new
    # map.log = true
    map.explore do |cmd|
      chmove.send cmd + 1
      chstatus.receive
    end
    # map.log = true
    map.find_way(map.start) { |pt| pt == map.final }.size
  end

  def part2(input)
    prg = input.split(',').map(&.to_i)
    chstatus = Channel(Int32).new
    chmove = Channel(Int32).new

    spawn { IntCode.async(prg, chmove, chstatus) }

    map = Map.new
    # map.log = true
    map.explore do |cmd|
      chmove.send cmd + 1
      chstatus.receive
    end
    # map.log = true
    map.find_way(map.final.not_nil!) { false }
    map.maxdist
  end
end
