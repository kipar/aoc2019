require "./aoc2019"

module Day6
  include Day
  extend self

  def to_list(input)
    input.split('\n').map do |x|
      _, first, second = /(.*)\)(.*)/.match(x.chomp).not_nil!
      {first, second}
    end
  end

  def part1(input)
    centerof = Hash(String, String).new
    norbits = {"COM" => 0}
    to_list(input).each do |first, second|
      centerof[second] = first
    end
    sum = 0
    centerof.each do |satellite, center|
      node = center
      level = 0
      while !norbits[node]?
        node = centerof[node]
        level += 1
      end
      level += norbits[node]
      norbits[satellite] = level + 1
      node = center
      while !norbits[node]?
        norbits[node] = level
        node = centerof[node]
        level -= 1
      end
    end
    norbits.values.sum
  end

  def part2(input)
    centerof = Hash(String, String).new
    to_list(input).each do |first, second|
      centerof[second] = first
    end
    distance = Hash(String, Int32).new
    node = centerof["YOU"]
    cur = 0
    loop do
      distance[node] = cur
      break if node == "COM"
      cur += 1
      node = centerof[node]
    end
    node = centerof["SAN"]
    dist2 = 0
    while !distance[node]?
      node = centerof[node]
      dist2 += 1
    end
    distance[node] + dist2
  end
end
