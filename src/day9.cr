require "./aoc2019"

module Day9
  include Day
  extend self

  def part1(input)
    IntCode.execute(input.split(',').map(&.to_i64), [1i64])
  end

  def part2(input)
    IntCode.execute(input.split(',').map(&.to_i64), [2i64])
  end
end
