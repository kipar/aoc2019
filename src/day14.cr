require "./aoc2019"

module Day14
  include Day
  extend self

  class Substance
    getter name : String
    getter required_for = [] of Substance
    getter reaction = {} of Substance => Int64
    property reaction_produced = 0i64
    property! requested : Int64? # counted in <reaction_produced> boxes
    # property remains = 0

    def initialize(@name)
    end

    def reset
      @requested = nil
    end
  end

  def parse(input)
    pool = Hash(String, Substance).new { |x, name| x[name] = Substance.new name }
    input.each_line do |react|
      _, components, output, name = /(.*) => (.*) (.*)/.match(react.strip).not_nil!
      result = pool[name]
      result.reaction_produced = output.to_i64
      components.split(',') do |comp|
        _, count, subs = /(.*) (.*)/.match(comp).not_nil!
        subs = pool[subs]
        result.reaction[subs] = count.to_i64
        subs.required_for << result
      end
    end
    pool
  end

  def solve(pool)
    while !pool["ORE"].requested?
      subs = (pool.find { |(name, x)| (!x.requested?) && x.required_for.all?(&.requested?) }.not_nil!)[1]

      # puts "processing #{subs.name}"
      subs.requested = subs.required_for.sum do |application|
        # puts "...required #{application.requested} * #{application.reaction[subs]} = #{application.requested * application.reaction[subs]} for #{application.name}"
        application.requested * application.reaction[subs]
      end
      # puts "...total: #{subs.requested}"
      if subs.reaction_produced != 0
        subs.requested = subs.requested // subs.reaction_produced + (subs.requested % subs.reaction_produced != 0 ? 1 : 0)
      end
      # puts "...or #{subs.requested} portions"
    end
  end

  def part1(input)
    pool = parse(input)
    # now solve
    pool["FUEL"].requested = 1
    solve(pool)
    pool["ORE"].requested
  end

  def part2(input)
    pool = parse(input)
    # now solve for 1
    pool["FUEL"].requested = 1
    solve(pool)
    pack = pool["ORE"].requested
    #
    # now solve for full part
    (((1e12 // pack).to_i..Int32::MAX).bsearch do |x|
      pool.each &.[1].reset
      pool["FUEL"].requested = x.to_i64
      solve(pool)
      pool["ORE"].requested > 1e12
    end).not_nil! - 1
  end
end
