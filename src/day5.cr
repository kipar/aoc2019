require "./aoc2019"
require "./intcode"

module Day5
  include Day
  extend self

  def to_list(input)
    input.split(',').map(&.to_i)
  end

  def part1(input)
    IntCode.execute(to_list(input), [1])
  end

  def part2(input)
    IntCode.execute(to_list(input), [5])
  end
end
