require "./aoc2019"

module Day10
  include Day
  extend self

  class Map
    getter width : Int32 = 1
    getter height : Int32

    def initialize(input)
      list = input.split('\n')
      @width = list[0].chomp.size
      @height = list.size
      @data = Array(Bool).new(@width * @height)
      @visible = Array(Int32).new(@width * @height, 0)
      @angles = Array(Tuple(Int32, Int32, Float64)).new
      list.each do |line|
        line.chomp.each_char do |char|
          @data << (char == '#')
        end
      end
    end

    def inmap(x, y)
      (0...@width).includes?(x) && (0...@height).includes?(y)
    end

    def raycast(x0, y0, dx, dy)
      # while inmap(x0, y0)
      #   incv(x0, y0)
      #   x0, y0 = x0 + dx, y0 + dy
      # end
      # return

      # find first asteroid
      while !asteroid(x0, y0)
        x0, y0 = x0 + dx, y0 + dy
        return unless inmap(x0, y0)
      end

      # find last point
      rx = dx < 0 ? x0 : @width - x0 - 1
      ry = dy < 0 ? y0 : @height - y0 - 1

      if dx == 0
        steps = ry // dy.abs
      elsif dy == 0
        steps = rx // dx.abs
      else
        steps = {rx // dx.abs, ry // dy.abs}.min
      end
      return if steps == 0
      xlast = x0 + dx * steps
      ylast = y0 + dy * steps
      # step back to get asteroid
      while !asteroid(xlast, ylast)
        xlast, ylast = xlast - dx, ylast - dy
        steps -= 1
        return if steps == 0
      end
      # now edges see one and between them see two
      incv(x0, y0)
      incv(xlast, ylast)
      x, y = x0 + dx, y0 + dy
      (steps - 1).times do
        if asteroid(x, y)
          incv(x, y)
          incv(x, y)
        end
        x, y = x + dx, y + dy
      end
    end

    def direct_check(x0, y0, mindelta)
      (-x0..@width - x0 - 1).each do |dx|
        next if dx.abs == 0
        (-y0..@height - y0 - 1).each do |dy|
          next if dx.abs + dy.abs <= mindelta
          next if dx.gcd(dy) > 1
          x, y = x0 + dx, y0 + dy
          loop do
            if asteroid(x, y)
              incv(x0, y0)
              break
            end
            x, y = x + dx, y + dy
            break unless inmap(x, y)
          end
        end
      end
    end

    def visibility
      # draw all vertical lines
      @width.times do |x|
        raycast(x, 0, 0, 1)
      end
      # horizontal lines
      @height.times do |y|
        raycast(0, y, 1, 0)
      end

      (@width - 1).times do |x|
        raycast(x, 0, 1, 1)
        raycast(x + 1, 0, -1, 1)
      end
      (@height - 2).times do |y|
        raycast(0, y + 1, 1, 1)
        raycast(@width - 1, y + 1, -1, 1)
      end

      @height.times do |y|
        @width.times do |x|
          direct_check(x, y, 2) if asteroid(x, y)
        end
      end
    end

    def part1
      n = (0...@data.size).max_by { |i| @visible[i] }
      {n % @width, n // @width, @visible[n]}
    end

    def angle_check(x0, y0)
      @angles.clear
      (-x0..@width - x0 - 1).each do |dx|
        (-y0..@height - y0 - 1).each do |dy|
          next if dx.abs + dy.abs == 0
          next if dx.gcd(dy) > 1
          x, y = x0 + dx, y0 + dy
          loop do
            if asteroid(x, y)
              @angles << {x, y, Math.atan2(y0 - y, x - x0)}
              break
            end
            x, y = x + dx, y + dy
            break unless inmap(x, y)
          end
        end
      end
    end

    def clockangle(angle)
      a = Math::PI/2 - angle
      a += 2*Math::PI if a < 0
      a
    end

    def check2(x0, y0, count)
      angle_check(x0, y0)
      if @angles.size < count
        # TODO - vapourize
      else
        @angles.sort_by! { |(x, y, a)| clockangle(a) }
        v = @angles[count - 1]
        {v[0], v[1]}
      end
    end

    def asteroid(x, y)
      @data[x + @width*y]
    end

    def incv(x, y)
      @visible[x + @width*y] += 1
    end

    def draw
      puts "#{@width}x#{@height}"
      puts(String.build do |str|
        @height.times do |y|
          @width.times do |x|
            # str << (@data[read_order(x, y)].asteroid ? "#" : ".")
            str << (@visible[x + @width*y] % 10)
          end
          str << "\n"
        end
      end)
    end
  end

  def part1(input)
    map = Map.new(input)
    map.visibility
    # map.draw
    map.part1
  end

  def part2(input)
    map = Map.new(input)
    map.visibility
    point = map.part1
    value = map.check2(point[0], point[1], 200).not_nil!
    value[0]*100 + value[1]
  end
end
