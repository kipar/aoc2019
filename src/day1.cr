require "./aoc2019"

module Day1
  include Day
  extend self

  def to_list(input)
    input.split(/[,\n]/).map(&.to_i)
  end

  def part1(input)
    to_list(input).sum { |x| x // 3 - 2 }
  end

  def part2(input)
    to_list(input).sum do |x|
      sum = 0
      loop do
        x = x // 3 - 2
        break if x <= 0
        sum += x
      end
      sum
    end
  end
end
