require "./aoc2019"

module Day16
  include Day
  extend self

  BASE = [0, 1, 0, -1]

  def fft(input, n)
    digits = input.chars.map &.to_i
    d2 = digits.clone
    n.times do
      d2.map_with_index! do |_, i|
        sum = 0
        digits.size.times do |j|
          c = digits[j]
          digit = ((j + 1) // (i + 1) + BASE.size) % BASE.size
          sum += c*BASE[digit]
        end
        sum.abs % 10
      end
      digits.replace(d2)
    end
    digits.map(&.to_s).join
  end

  def part1(input)
    fft(input, 100)[0...8]
  end

  def part2(input)
    "part2"
  end
end
