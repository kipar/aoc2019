module IntCode
  class Program(T)
    def initialize(@data : Array(T))
    end

    def []=(addr, value)
      if addr < @data.size
        @data[addr] = value.as(T)
      else
        # puts "write at addr: #{addr}"
        @data = @data + Array(T).new(addr - @data.size + 1) { T.new(0) }
        @data[addr] = value.as(T)
      end
    end

    def [](addr)
      if addr < @data.size
        @data[addr]
      else
        # puts "read at addr: #{addr}"
        T.new(0)
      end
    end
  end

  def self.async(program, inputch, outputch)
    step = 0
    relative_base = 0
    program = Program.new(program)
    loop do
      op = program[step]
      code = op % 100
      modes = [(op // 100) % 10, (op // 1000) % 10, (op // 10000) % 10]
      arg_count, jump, has_result = case code
                                    when 1, 2; {2, false, true}
                                    when 3   ; {0, false, true}
                                    when 4   ; {1, false, false}
                                    when 5, 6; {2, true, false}
                                    when 7, 8; {2, false, true}
                                    when 9   ; {1, false, false}
                                    when 99  ; return
                                    else       raise "incorrect opcode at step #{step}: #{code}"
                                    end
      args = (1..arg_count).to_a.map do |i|
        v = program[step + i]
        case modes[i - 1]
        when 0; program[v]
        when 1; v
        when 2; program[v + relative_base]
        else    raise "incorrect modes at step #{step}: #{modes}"
        end
      end
      res_addr = program[step + arg_count + 1] if has_result
      step += arg_count + (has_result ? 2 : 1)
      case code
      when 1; result = args[0] + args[1]
      when 2; result = args[0] * args[1]
      when 3; result = inputch.receive if inputch
      when 4; outputch.send args[0] if outputch
      when 5; step = args[1] if args[0] != 0
      when 6; step = args[1] if args[0] == 0
      when 7; result = args[0] < args[1] ? typeof(program[0]).new(1) : typeof(program[0]).new(0)
      when 8; result = args[0] == args[1] ? typeof(program[0]).new(1) : typeof(program[0]).new(0)
      when 9; relative_base += args[0]
      end
      if has_result
        case modes[arg_count]
        when 0; program[res_addr.not_nil!] = result.not_nil!
        when 2; program[res_addr.not_nil! + relative_base] = result.not_nil!
        else    raise "incorrect mode of result: #{modes[arg_count]}"
        end
      end
    end
  end

  def self.execute(program, input = nil)
    outputch = Channel(typeof(program[0])).new(1)
    if input
      inputch = Channel(typeof(program[0])).new(input.size)
      input.each do |x|
        inputch.send x
      end
    else
      inputch = nil
    end
    spawn { async(program, inputch, outputch) }
    outputch.receive
  end

  def self.execute_arr(program, input = nil)
    outputch = Channel(typeof(program[0])).new
    stopped = Channel(Nil).new
    if input
      inputch = Channel(typeof(program[0])).new(input.size)
      input.each do |x|
        inputch.send x
      end
    else
      inputch = nil
    end
    spawn do
      async(program, inputch, outputch)
      stopped.send(nil)
    end
    output = [] of typeof(program[0])
    spawn do
      loop do
        output << outputch.receive
      end
    end
    stopped.receive
    output
  end
end
