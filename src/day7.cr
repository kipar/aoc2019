require "./aoc2019"
require "./intcode"

module Day7
  include Day
  extend self

  def part1(input)
    input = input.split(',').map(&.to_i)
    (0..4).to_a.permutations.map do |phases|
      v = 0
      phases.each do |phase|
        v = IntCode.execute(input.dup, [phase, v])
      end
      v
    end.max
  end

  def part2(input)
    program = input.split(',').map(&.to_i)
    stopped = Channel(Nil).new(5)
    (5..9).to_a.permutations.map do |phases|
      signals = Array(Channel(Int32)).new(6) { |i| Channel(Int32).new(2) }
      phases.each_with_index do |phase, i|
        signals[i].send Int32.new(phase)
        spawn { IntCode.async(program.dup, signals[i], signals[i + 1]); stopped.send nil }
      end
      last_input = Int32::MIN
      spawn do
        loop do
          last_input = signals[5].receive
          signals[0].send last_input
        end
      end
      signals[0].send 0
      5.times { stopped.receive }
      last_input
    end.max
  end
end
