require "./aoc2019"

module Day2
  include Day
  extend self

  def to_list(input)
    input.split(/[,\n]/).map(&.to_i)
  end

  def execute(list)
    list.tap do |prg|
      prg.each_slice(4, true) do |statement|
        op = statement[0]
        case op
        when 1
          arg1, arg2, result = statement[1..]
          prg[result] = prg[arg1] + prg[arg2]
        when 2
          arg1, arg2, result = statement[1..]
          prg[result] = prg[arg1] * prg[arg2]
        when 99
          break
        else
          raise "wrong opcode"
        end
      end
    end
  end

  def part1(input)
    eval(to_list(input), {12, 2})
  end

  def eval(prg, params)
    prg[1], prg[2] = params
    execute(prg)[0]
  end

  def part2(input)
    prg = to_list(File.read("./input/day2"))
    value = input.to_i
    params = [1, 1]
    100.times do |i|
      100.times do |j|
        begin
          v = eval(prg.clone, {i, j})
          return i*100 + j if v == value
        rescue
        end
      end
    end
  end
end
