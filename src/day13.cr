require "ncurses"
require "./aoc2019"

module Day13
  include Day
  extend self

  def part1(input)
    chdraw = Channel(Int64).new
    chdone = Channel(Nil).new
    spawn do
      IntCode.async(input.split(',').map(&.to_i64), nil, chdraw)
      chdone.send(nil)
    end
    map = Hash(Tuple(Int64, Int64), Int64).new
    spawn do
      loop do
        x = chdraw.receive
        y = chdraw.receive
        tile = chdraw.receive
        map[{x, y}] = tile
      end
    end
    chdone.receive
    p map.values.count(2), map.minmax_by { |k, v| k[0] }, map.minmax_by { |k, v| k[1] }
  end

  MAXX     = 44
  MAXY     = 24
  HANDMODE = true

  class Game
    @ai = Array(Tuple(Int32, Int64)).new
    @ballx = 0i64
    @padx = 0i64
    @prevballx = 0i64
    @manual = false

    def do_ai(prev, time)
      return 1 if time < 2
      @ai.each do |(tick, pos)|
        next if tick < time
        return 0 if (tick - time) > (@padx - pos).abs + 3
        return 0 if (pos - @padx).abs < 2 && prev != 0
        return pos <=> @padx
      end
      @manual = true if HANDMODE
      0
    end

    def initialize
      NCurses.start
      @draw = NCurses::Window.new(NCurses.height, 50, 0, 0)
      @log = NCurses::Window.new(NCurses.height, 50, 0, 55)
    end

    def train(input)
      @ai << {100, 23i64}
      @ai << {409, 3i64}
      @ai << {891, 19i64}
      @ai << {1181, 27i64}
      @ai << {1281, 31i64}
      @ai << {1297, 37i64}
      @ai << {1521, 33i64}
      @ai << {1639, 3i64}
      @ai << {1719, 9i64}
      @ai << {1819, 7i64}
      @ai << {1949, 5i64}
      @ai << {2025, 9i64}
      @ai << {2065, 13i64}
      @ai << {2141, 11i64}
      @ai << {2191, 9i64}
      @ai << {2355, 29i64}
      @ai << {2395, 14i64}

      @ai << {2439, 19i64}
      @ai << {2537, 15i64}
      @ai << {2719, 23i64}
      @ai << {2831, 20i64}
      @ai << {2885, 23i64}
      # @ai << {2925, 19i64}

      prevsc = 0
      lines = [] of String
      loop do
        sc, tick, oursc = game(input)
        @log.move 1, 1
        @log.print "GAMEOVER AT #{tick}: #{sc}, ballx=#{@ballx}, remain=#{oursc}"
        # @draw.refresh
        @log.refresh
        # NCurses.get_char
        pos = @ballx - (@ballx - @prevballx)
        if oursc != prevsc || @ai[@ai.size - 2][1] != pos
          @ai << {tick, pos}
          lines << "@ai << {#{tick}, #{pos}i64}"
        else
          # v = @ai[@ai.size - 1]
          # @ai[@ai.size - 1] = {v[0], v[1] - (@ballx - @prevballx)}
          lines << "(stall)@ai << {#{tick}, #{pos}i64}"
        end
        prevsc = oursc

        lines.each_with_index do |item, i|
          @log.move (i % 20) + 2, 12*(i // 20)
          @log.print item
        end
        @log.refresh
        NCurses.get_char
      end
    end

    def prognose(map, prevballx, prevbally, ballx, bally)
      dx, dy = ballx <=> prevballx, bally <=> prevbally
      x, y = ballx, bally
      loop do
        puts({x, y, dx, dy})
        map[x + MAXX*y] = '*'
        dx = -dx if x == 1 || x == MAXX - 2
        dy = -dy if y == 1
        x, y = x + dx, y + dy
        break if y > 23 || map[x + MAXX*y] == '#'
      end
      map[ballx + MAXX*bally] = '@'
    end

    def game(input)
      chdraw = Channel(Int64).new
      chdone = Channel(Nil).new
      chjoy = Channel(Int64).new
      prg = input.split(',').map(&.to_i64)
      prg[0] = 2
      spawn do
        IntCode.async(prg, chjoy, chdraw)
        chdone.send(nil)
      end
      map = Array(Char).new(MAXX*MAXY, '?')
      score = 0
      tick = 0
      @padx = 0i64
      @ballx = 0i64
      bally = 0
      @prevballx = 0
      prevbally = 0
      prev = 0
      spawn do
        loop do
          x = chdraw.receive
          y = chdraw.receive
          tile = chdraw.receive
          if if x < 0
               score = tile
               p score
             else
               case tile
               when 3
                 @padx = x
               when 4
                 @prevballx = @ballx unless @prevballx == @ballx
                 prevbally = bally unless prevbally == bally
                 @ballx = x
                 bally = y
               end
               map[x + MAXX*y] = {'.', '█', '#', '-', 'o'}[tile]
             end
          end
        end
      end

      # NCurses.start
      # NCurses.no_echo
      lastscore = 0
      spawn do
        loop do
          while @padx == 0 || @ballx == 0
            Fiber.yield
          end
          @draw.clear
          map1 = map.clone
          prognose(map1, @prevballx, prevbally, @ballx, bally) if @manual
          MAXX.times do |x|
            MAXY.times do |y|
              @draw.move y, x
              @draw.print map1[x + MAXX*y].to_s
            end
          end
          @draw.move 0, 0
          @draw.print "#{score}, #{@padx}, #{score - lastscore}"
          lastscore = score
          @draw.refresh
          tick += 1
          if @manual
            v = case NCurses.get_char
                when 'a'; -1
                when 'd'; 1
                else      0
                end
          else
            v = do_ai(prev, tick)
          end
          # NCurses.get_char if bally > 20

          prev = v
          # v = NCurses.get_char
          chjoy.send v.to_i64
        end
      end

      chdone.receive

      # NCurses.end
      {score, tick, map.count('#')}
    end
  end

  def part2(input)
    Game.new.train(input)
  end
end

# 23
# 3
# 19
# 27
